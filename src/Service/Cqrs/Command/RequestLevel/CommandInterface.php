<?php
namespace App\Service\Cqrs\Command\RequestLevel;

use App\Service\Cqrs\Command\CommandInterface as BaseCommandInterface;
use Symfony\Component\HttpFoundation\Request;

interface CommandInterface extends BaseCommandInterface
{

	/**
	 * @param Request $request
	 * @param null|int $numStep
	 */
	public function setItem(Request $request, ?int $numStep): void;

	/**
	 * @return array
	 */
	public function getItems(): array;

}
