<?php
namespace App\Service\Cqrs\Command\DocumentLog\Factory;

use App\Service\Cqrs\Command\DocumentLog\Factory\CommandFactoryInterface;
use App\Service\Cqrs\Command\DocumentLog\EventsInterface;
use App\Entity\Account;
use App\Entity\Document;
use App\Entity\DocumentAccount;
use App\Service\Cqrs\Command\DocumentLog\CommandInterface;
use App\Service\Cqrs\Command\DocumentLog\Command;
use App\Service\Db\Helpers\Factory\DateTimeFactoryInterface;
use App\Service\Db\Helpers\Factory\DateTimeFactory;

class CommandFactory implements CommandFactoryInterface
{
	/** @var EventsInterface */
	protected $events;
	/** @var DateTimeFactoryInterface */
	protected $dateTimeFactory;

	/**
	 * @param EventsInterface $events
	 * @param DateTimeFactoryInterface $dateTimeFactory
	 */
	public function __construct(EventsInterface $events, DateTimeFactoryInterface $dateTimeFactory)
	{
		$this->events = $events;
		$this->dateTimeFactory = $dateTimeFactory;
	}

	/**
	 * @inheritDoc
	 */
	public function factory(Account $user, ?DocumentAccount $documentAccount, Document $document, string $eventKey, int $status): CommandInterface
	{
		$now = $this->dateTimeFactory->factory(DateTimeFactory::DEFAULT_DATETIME);

		return new Command(
			$user,
			$documentAccount,
			$document,
			$eventKey,
			$this->events->getMessage($eventKey),
			$status,
			$now,
			$now
		);
	}

}
