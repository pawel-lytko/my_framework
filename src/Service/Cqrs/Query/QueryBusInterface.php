<?php
namespace App\Service\Cqrs\Query;

use App\Service\Cqrs\Query\QueryInterface;
use App\Service\Cqrs\Query\QueryHandlerInterface;

interface QueryBusInterface
{
	/**
	 * @param QueryHandlerInterface $queryHandler
	 *
	 * @return QueryBusInterface
	 */
	public function registerHandle(QueryHandlerInterface $queryHandler): QueryBusInterface;

	/**
	 * @param QueryInterface $query
	 *
	 * @return mixed
	 * @throws \App\Exception\ObjectExistsException
	 */
	public function execute(QueryInterface $query);

}
