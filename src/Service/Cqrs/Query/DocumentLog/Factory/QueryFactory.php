<?php
namespace App\Service\Cqrs\Query\DocumentLog\Factory;

use App\Service\Cqrs\Query\DocumentLog\Factory\QueryFactoryInterface;
use App\Service\Cqrs\Query\DocumentLog\QueryInterface;
use App\Service\Cqrs\Query\DocumentLog\Query;

class QueryFactory implements QueryFactoryInterface
{
	/**
	 * @inheritDoc
	 */
	public function factory(int $documentId): QueryInterface
	{
		return new Query(
			$documentId,
			'dl.event_date_time ASC'
		);
	}

}
