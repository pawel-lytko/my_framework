<?php
namespace App\Service\Cqrs\Command\DocumentLog;

interface EventsInterface
{

	/**
	 * @param string $key
	 *
	 * @return null|string
	 */
	public function getMessage(string $key): ?string;

}
