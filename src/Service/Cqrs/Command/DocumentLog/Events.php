<?php
namespace App\Service\Cqrs\Command\DocumentLog;

use App\Service\Cqrs\Command\DocumentLog\EventsInterface;

class Events implements EventsInterface
{
	public const CREATE_DOCUMENT_KEY = 'create_document';
	public const DOWNLOAD_DOCUMENT_KEY = 'download_document';
	public const SEND_MAIL_DOCUMENT_KEY = 'send_mail_document';
	public const SIGN_DOCUMENT_KEY = 'sign_document';

	/**
	 * @inheritDoc
	 */
	public function getMessage(string $key): ?string
	{
		$message = [
			static::CREATE_DOCUMENT_KEY => 'Dokument utworzony',
			static::DOWNLOAD_DOCUMENT_KEY => 'Dokumentu dodany',
			static::SEND_MAIL_DOCUMENT_KEY => 'Dokument wysłany do uczestnika',
			static::SIGN_DOCUMENT_KEY => 'Dokument podpisany',
		];

		return (isset($message[$key]) ? $message[$key] : null);
	}

}
