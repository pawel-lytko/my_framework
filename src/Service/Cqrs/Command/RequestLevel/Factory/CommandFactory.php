<?php
namespace App\Service\Cqrs\Command\RequestLevel\Factory;

use App\Service\Cqrs\Command\RequestLevel\Factory\CommandFactoryInterface;
use App\Service\Cqrs\Command\RequestLevel\CommandInterface;
use App\Service\Cqrs\Command\RequestLevel\Command;

class CommandFactory implements CommandFactoryInterface
{
	/**
	 * @inheritDoc
	 */
	public function factory(array $data): CommandInterface
	{
		return new Command($data);
	}

}
