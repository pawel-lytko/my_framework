<?php
namespace App\Service\Cqrs\Command\RequestLevel\Factory;

use App\Service\Cqrs\Command\RequestLevel\CommandInterface;

interface CommandFactoryInterface
{

	/**
	 * @param array $data
	 *
	 * @return CommandInterface
	 */
	public function factory(array $data): CommandInterface;

}
