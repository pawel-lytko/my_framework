<?php
namespace App\Service\Cqrs\Query\DocumentLog\Factory;

use App\Service\Cqrs\Query\DocumentLog\QueryInterface;

interface QueryFactoryInterface
{

	/**
	 * @param int $documentId
	 *
	 * @return QueryInterface
	 */
	public function factory(int $documentId): QueryInterface;

}
