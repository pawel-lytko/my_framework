<?php
namespace App\Service\Cqrs\Query\DocumentLog;

use App\Service\Cqrs\Query\QueryInterface as CoreQueryInterface;

interface QueryInterface extends CoreQueryInterface
{

	/**
	 * @return int
	 */
	public function getDocumentId(): int;

	/**
	 * @return string
	 */
	public function getOrderBy(): string;

}
