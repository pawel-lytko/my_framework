<?php
namespace App\Service\Cqrs\Query\DocumentLog;

use App\Service\Cqrs\Query\QueryHandlerInterface as CoreQueryHandlerInterface;
use App\Service\Cqrs\Query\QueryInterface;

interface QueryHandlerInterface extends CoreQueryHandlerInterface
{
	/**
	 * @param QueryInterface $query
	 *
	 * @return array
	 */
	public function handle(QueryInterface $query): array;
}
