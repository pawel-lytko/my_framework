<?php
namespace App\Service\Cqrs\Command\DocumentLog;

use App\Service\Cqrs\Command\CommandHandlerInterface;
use App\Service\Cqrs\Command\CommandInterface;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\DocumentLog;

class CommandHandler implements CommandHandlerInterface
{
	/** @var ManagerRegistry */
	protected $manager;
	/** @var DocumentLog */
	protected $documentLog;
	protected $insertAction;

	/**
	 * @param ManagerRegistry $manager
	 * @param DocumentLog $documentLog
	 * @param bool $insertAction
	 */
	public function __construct(ManagerRegistry $manager, DocumentLog $documentLog, bool $insertAction)
	{
		$this->manager = $manager;
		$this->documentLog = $documentLog;
		$this->insertAction = $insertAction;
	}

	/**
	 * @param \App\Service\Cqrs\Command\DocumentLog\CommandInterface $command
	 */
	public function handle(CommandInterface $command): void
	{
		$this->documentLog
			->setModified($command->getDbDateTime());

		if ($this->insertAction) {
			$this->documentLog
				->setCreated($command->getDbDateTime());
		}

		$this->documentLog
			->setMsg($command->getMessage())
			->setUser($command->getUser())
			->setDocumentAccount($command->getDocumentAccount())
			->setEventDateTime($command->getEventDateTime())
			->setEventKey($command->getEventKey())
			->setStatus($command->getStatus())
			->setDocument($command->getDocument());

		$this->manager->getManager()->persist($this->documentLog);
		$this->manager->getManager()->flush($this->documentLog);
	}
}
