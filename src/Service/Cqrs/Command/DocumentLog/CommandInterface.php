<?php
namespace App\Service\Cqrs\Command\DocumentLog;

use App\Entity\DocumentAccount;
use App\Service\Cqrs\Command\CommandInterface as BaseCommandInterface;
use DateTimeInterface as CoreDateTimeInterface;
use App\Entity\Account;
use App\Entity\Document;

interface CommandInterface extends BaseCommandInterface
{

	/**
	 * @return CoreDateTimeInterface
	 */
	public function getDbDateTime(): CoreDateTimeInterface;

	/**
	 * @return null|string
	 */
	public function getMessage(): ?string;

	/**
	 * @return Account
	 */
	public function getUser(): Account;

	/**
	 * @return null|DocumentAccount
	 */
	public function getDocumentAccount(): ?DocumentAccount;

	/**
	 * @return Document
	 */
	public function getDocument(): Document;

	/**
	 * @return CoreDateTimeInterface
	 */
	public function getEventDateTime(): CoreDateTimeInterface;

	/**
	 * @return string
	 */
	public function getEventKey(): string;

	/**
	 * @return int
	 */
	public function getStatus(): int;

}
