<?php
namespace App\Service\Cqrs\Command;

use App\Service\Cqrs\Command\CommandInterface;

interface CommandHandlerInterface
{
	/**
	 * @param CommandInterface $command
	 */
	public function handle(CommandInterface $command): void;
}
