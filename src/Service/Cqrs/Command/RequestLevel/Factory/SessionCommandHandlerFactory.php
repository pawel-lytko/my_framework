<?php
namespace App\Service\Cqrs\Command\RequestLevel\Factory;

use App\Service\Cqrs\Command\RequestLevel\Factory\SessionCommandHandlerFactoryInterface;
use App\Service\Cqrs\Command\CommandHandlerInterface;
use App\Service\Cqrs\Command\RequestLevel\SessionCommandHandler;
use Symfony\Component\HttpFoundation\Request;

class SessionCommandHandlerFactory implements SessionCommandHandlerFactoryInterface
{
	/**
	 * @inheritDoc
	 */
	public function factory(Request $request): CommandHandlerInterface
	{
		return new SessionCommandHandler($request);
	}

}
