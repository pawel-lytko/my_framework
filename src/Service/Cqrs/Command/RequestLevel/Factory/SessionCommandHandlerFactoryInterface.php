<?php
namespace App\Service\Cqrs\Command\RequestLevel\Factory;

use App\Service\Cqrs\Command\CommandHandlerInterface;
use Symfony\Component\HttpFoundation\Request;

interface SessionCommandHandlerFactoryInterface
{

	/**
	 * @param Request $request
	 *
	 * @return CommandHandlerInterface
	 */
	public function factory(Request $request): CommandHandlerInterface;

}
