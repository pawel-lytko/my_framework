<?php
namespace App\Service\Cqrs\Command;

use App\Service\Cqrs\Command\CommandBusInterface;
use App\Service\Cqrs\Command\CommandHandlerInterface;
use App\Service\Cqrs\Command\CommandInterface;

class CommandBus implements CommandBusInterface
{
	/** @var array */
	protected $handles;

	public function __construct()
	{
		$this->handles = [];
	}

	/**
	 * @inheritDoc
	 */
	public function reset(): CommandBusInterface
	{
		unset($this->handles);
		$this->handles = [];

		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function registerHandle(CommandHandlerInterface $commandHandler): CommandBusInterface
	{
		$this->handles[] = $commandHandler;

		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function dispatch(CommandInterface $command): void
	{
		/** @var CommandHandlerInterface $handle */
		foreach ($this->handles as $handle) {
			$handle->handle($command);
		}
	}

}
