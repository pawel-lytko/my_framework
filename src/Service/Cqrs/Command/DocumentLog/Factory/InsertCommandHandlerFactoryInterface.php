<?php
namespace App\Service\Cqrs\Command\DocumentLog\Factory;

use App\Service\Cqrs\Command\CommandHandlerInterface;
use Doctrine\Persistence\ManagerRegistry;

interface InsertCommandHandlerFactoryInterface
{

	/**
	 * @param ManagerRegistry $manager
	 *
	 * @return CommandHandlerInterface
	 */
	public function factory(ManagerRegistry $manager): CommandHandlerInterface;

}
