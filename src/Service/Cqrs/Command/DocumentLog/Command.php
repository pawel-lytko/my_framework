<?php
namespace App\Service\Cqrs\Command\DocumentLog;

use App\Service\Cqrs\Command\DocumentLog\CommandInterface;
use App\Entity\DocumentAccount;
use App\Entity\Account;
use App\Entity\Document;
use DateTimeInterface as CoreDateTimeInterface;

class Command implements CommandInterface
{
	public const SUCCESS_STATUS = 1;
	public const EXCEPTION_STATUS = 2;

	/** @var Account */
	protected $user;
	/** @var Document */
	protected $document;
	/** @var string */
	protected $eventKey;
	/** @var null|string */
	protected $message;
	/** @var int */
	protected $status;
	/** @var CoreDateTimeInterface */
	protected $eventDateTime;
	/** @var CoreDateTimeInterface */
	protected $dbDateTime;
	/** @var null|DocumentAccount */
	protected $documentAccount;

	/**
	 * @param Account $user
	 * @param null|DocumentAccount $documentAccount
	 * @param Document $document
	 * @param string $eventKey
	 * @param null|string $message
	 * @param int $status
	 * @param CoreDateTimeInterface $eventDateTime
	 * @param CoreDateTimeInterface $dbDateTime
	 */
	public function __construct(Account $user,
								?DocumentAccount $documentAccount,
								Document $document,
								string $eventKey,
								?string $message,
								int $status,
								CoreDateTimeInterface $eventDateTime,
								CoreDateTimeInterface $dbDateTime)
	{
		$this->user = $user;
		$this->documentAccount = $documentAccount;
		$this->document = $document;
		$this->eventKey = $eventKey;
		$this->message = $message;
		$this->status = $status;
		$this->eventDateTime = $eventDateTime;
		$this->dbDateTime = $dbDateTime;
	}

	/**
	 * @inheritDoc
	 */
	public function getDbDateTime(): CoreDateTimeInterface
	{
		return $this->dbDateTime;
	}

	/**
	 * @inheritDoc
	 */
	public function getMessage(): ?string
	{
		return $this->message;
	}

	/**
	 * @inheritDoc
	 */
	public function getUser(): Account
	{
		return $this->user;
	}

	/**
	 * @inheritDoc
	 */
	public function getDocumentAccount(): ?DocumentAccount
	{
		return $this->documentAccount;
	}

	/**
	 * @inheritDoc
	 */
	public function getDocument(): Document
	{
		return $this->document;
	}

	/**
	 * @inheritDoc
	 */
	public function getEventDateTime(): CoreDateTimeInterface
	{
		return $this->eventDateTime;
	}

	/**
	 * @inheritDoc
	 */
	public function getEventKey(): string
	{
		return $this->eventKey;
	}

	/**
	 * @inheritDoc
	 */
	public function getStatus(): int
	{
		return $this->status;
	}

}
