<?php
namespace App\Service\Cqrs\Command\RequestLevel;

use App\Service\Cqrs\Command\CommandHandlerInterface;
use App\Service\Cqrs\Command\CommandInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Service\Cqrs\Query\RequestLevel\Query;

class SessionCommandHandler implements CommandHandlerInterface
{
	/** @var Request */
	protected $request;

	/**
	 * @param Request $request
	 */
	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	/**
	 * @param \App\Service\Cqrs\Command\RequestLevel\CommandInterface $command
	 */
	public function handle(CommandInterface $command): void
	{
		if (empty($command->getItems())) {
			$this->request->getSession()->remove(Query::CREATE_MEET_KEY);
		} else {
			$this->request->getSession()->set(Query::CREATE_MEET_KEY, serialize($command->getItems()));
		}
	}

}
