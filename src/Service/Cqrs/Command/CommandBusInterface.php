<?php
namespace App\Service\Cqrs\Command;

use App\Service\Cqrs\Command\CommandInterface;
use App\Service\Cqrs\Command\CommandHandlerInterface;

interface CommandBusInterface
{
	/**
	 * @return CommandBusInterface
	 */
	public function reset(): CommandBusInterface;

	/**
	 * @param CommandHandlerInterface $commandHandler
	 *
	 * @return CommandBusInterface
	 */
	public function registerHandle(CommandHandlerInterface $commandHandler): CommandBusInterface;

	/**
	 * @param CommandInterface $command
	 */
	public function dispatch(CommandInterface $command): void;

}
