<?php
namespace App\Service\Cqrs\Query;

use App\Service\Cqrs\Query\QueryBusInterface;
use App\Exception\ObjectExistsException;

class QueryBus implements QueryBusInterface
{
	/** @var QueryHandlerInterface|null */
	protected $queryHandler;

	public function __construct()
	{
		$this->queryHandler = null;
	}

	/**
	 * @inheritDoc
	 */
	public function registerHandle(QueryHandlerInterface $queryHandler): QueryBusInterface
	{
		$this->queryHandler = $queryHandler;

		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function execute(QueryInterface $query)
	{
		if (!($this->queryHandler instanceof QueryHandlerInterface)) {
			throw new ObjectExistsException('None object exists [QueryHandler]');
		}

		return $this->queryHandler->handle($query);
	}

}
