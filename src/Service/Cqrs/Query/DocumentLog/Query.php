<?php
namespace App\Service\Cqrs\Query\DocumentLog;

use App\Service\Cqrs\Query\DocumentLog\QueryInterface;

class Query implements QueryInterface
{
	/** @var int */
	protected $documentId;
	/** @var string */
	protected $orderBy;

	/**
	 * @param int $documentId
	 * @param string $orderBy
	 */
	public function __construct(int $documentId, string $orderBy)
	{
		$this->documentId = $documentId;
		$this->orderBy = $orderBy;
	}

	/**
	 * @inheritDoc
	 */
	public function getDocumentId(): int
	{
		return $this->documentId;
	}

	/**
	 * @inheritDoc
	 */
	public function getOrderBy(): string
	{
		return $this->orderBy;
	}

}
