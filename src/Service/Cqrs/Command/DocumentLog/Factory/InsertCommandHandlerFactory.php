<?php
namespace App\Service\Cqrs\Command\DocumentLog\Factory;

use App\Service\Cqrs\Command\DocumentLog\Factory\InsertCommandHandlerFactoryInterface;
use App\Service\Cqrs\Command\CommandHandlerInterface;
use Doctrine\Persistence\ManagerRegistry;
use App\Service\Cqrs\Command\DocumentLog\CommandHandler;
use App\Entity\DocumentLog;

class InsertCommandHandlerFactory implements InsertCommandHandlerFactoryInterface
{
	/**
	 * @inheritDoc
	 */
	public function factory(ManagerRegistry $manager): CommandHandlerInterface
	{
		return new CommandHandler(
			$manager,
			new DocumentLog(),
			true
		);
	}

}
