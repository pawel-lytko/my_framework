<?php
namespace App\Service\Cqrs\Query;

use App\Service\Db\Helpers\EntityInterface;
use Doctrine\ORM\QueryBuilder;

abstract class BaseHandler
{

	/** @var EntityInterface */
	protected $entityTools;
	/** @var QueryBuilder */
	protected $queryBuilder;

	/**
	 * @param EntityInterface $entityTools
	 * @param QueryBuilder $queryBuilder
	 */
	public function __construct(EntityInterface $entityTools, QueryBuilder $queryBuilder)
	{
		$this->entityTools = $entityTools;
		$this->queryBuilder = $queryBuilder;
	}

}
