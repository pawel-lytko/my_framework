<?php
namespace App\Service\Cqrs\Query\DocumentLog;

use App\Service\Cqrs\Query\DocumentLog\QueryHandlerInterface;
use App\Service\Cqrs\Query\QueryInterface;
use App\Service\Cqrs\Query\EntityManagerBase;
use Doctrine\DBAL\FetchMode;

class QueryHandler extends EntityManagerBase implements QueryHandlerInterface
{

	/**
	 * @inheritDoc
	 */
	public function handle(QueryInterface $query): array
	{
		/** @var \App\Service\Cqrs\Query\DocumentLog\QueryInterface $query */
		$query = $query;

		$sql = 'SELECT
					dl.user_id AS user_id
					, dl.document_account_id AS document_account_id
					, dl.event_date_time AS event_date_time
					, dl.msg AS message
					, a.first_name AS account_first_name
					, a.last_name AS account_last_name
					, da.first_name AS document_account_first_name
					, da.last_name AS document_account_last_name
				FROM
					document_log AS dl
				LEFT JOIN
					account AS a ON (dl.user_id = a.id)
				LEFT JOIN
					document_account AS da ON (dl.document_account_id = da.id)
				WHERE
					dl.document_id = :dl_document_id
				ORDER BY ' . $query->getOrderBy();

		$stmt = $this->entityManager->getConnection()->prepare($sql);

		$stmt->execute(['dl_document_id' => $query->getDocumentId()]);

		return $stmt->fetchAll();
	}

}
