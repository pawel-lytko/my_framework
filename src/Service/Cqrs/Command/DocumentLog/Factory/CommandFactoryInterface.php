<?php
namespace App\Service\Cqrs\Command\DocumentLog\Factory;

use App\Entity\Account;
use App\Entity\Document;
use App\Entity\DocumentAccount;
use App\Service\Cqrs\Command\DocumentLog\CommandInterface;

interface CommandFactoryInterface
{

	/**
	 * @param Account $user
	 * @param null|DocumentAccount $documentAccount
	 * @param Document $document
	 * @param string $eventKey
	 * @param int $status
	 *
	 * @return CommandInterface
	 */
	public function factory(Account $user,
							?DocumentAccount $documentAccount,
							Document $document,
							string $eventKey,
							int $status): CommandInterface;

}
