<?php
namespace App\Service\Cqrs\Command\RequestLevel;

use App\Service\Cqrs\Command\RequestLevel\CommandInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Validator\Definition\Zoom\Base;

class Command implements CommandInterface
{
	/** @var array */
	protected $data;

	/**
	 * @param array $data
	 */
	public function __construct(array $data)
	{
		$this->data = $data;
	}

	/**
	 * @param Request $request
	 *
	 * @return array
	 */
	protected function getRelations(Request $request): array
	{
		$tmpRelations = $request->request->get(Base::RELATION_KEY, []);
		$relations = [];

		foreach ($tmpRelations as $item) {
			[$key, $data] = explode(':', $item);
			$relations[$key] = $data;
		}

		return $relations;
	}

	/**
	 * @param Request $request
	 * @param array $relations
	 *
	 * @return null|string
	 */
	protected function getRelationOfRequest(Request $request, array $relations): ?string
	{
		if (is_array($request->request->get(Base::MEET_ENTITY_IDS_KEY))) {
			return null;
		}

		$id = $request->request->get(Base::MEET_ENTITY_IDS_KEY);

		return (isset($relations[$id]) ? $relations[$id] : null);
	}

	/**
	 * @inheritDoc
	 */
	public function setItem(Request $request, ?int $numStep): void
	{
		if ($numStep === null) {
			$this->data = [];
			return;
		}

		$this->data[$numStep] = [
			Base::RELATION_KEY => $this->getRelationOfRequest($request, $this->getRelations($request)),
			Base::CURRENT_LEVEL_KEY => $request->request->get(Base::CURRENT_LEVEL_KEY),
			Base::MEET_ENTITY_IDS_KEY => (
				is_array($request->request->get(Base::MEET_ENTITY_IDS_KEY)) ?
					array_values($request->request->get(Base::MEET_ENTITY_IDS_KEY)) :
					[$request->request->get(Base::MEET_ENTITY_IDS_KEY)]
			),
			Base::MEETING_DATE_KEY => $request->request->get(Base::MEETING_DATE_KEY),
			Base::MEETING_TIME_KEY => $request->request->get(Base::MEETING_TIME_KEY),
		];
	}

	/**
	 * @inheritDoc
	 */
	public function getItems(): array
	{
		return $this->data;
	}

}
